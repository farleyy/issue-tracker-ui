import React, { useEffect, useState } from 'react';
import './styles.css';
import { getIssues } from 'utils/requests';
import IssueElement from './IssueElement';

export const IssueList = () => {
    const [issues, setIssues] = useState([]);
    const [errorMessage, setErrorMessage] = useState();

    useEffect(() => {
        getIssues()
            .then(({data}) => setIssues(data.issues))
            .catch(() => setErrorMessage('Getting issue list failed!'))
    }, []);

    return (
        <section className="IssueList">
            <h1 className="IssueList__title">List of issues:</h1>

            {
                errorMessage && (
                    <span className="IssueList__errorMessage">
                        {errorMessage}
                    </span>
                )
            }

            <ul className="issue-list">
                {
                    issues.map(({id, title, description, state}) => <IssueElement
                        key={`issue-${id}`}
                        id={id}
                        title={title}
                        description={description}
                        state={state}
                    />)
                }
            </ul>

            
        </section>
    )
}