import React, { useState } from 'react';
import { patchIssue } from 'utils/requests';

const statuses = ['open', 'pending', 'closed'];

export default ({ id, title, description, state: oldStatus }) => {
    const [status, setStatus] = useState(oldStatus);
    const [errorMessage, setErrorMessage] = useState();

    const onChange = ({ target }) => {
        setErrorMessage();
        sendPatchRequest(target.value);
    };

    const sendPatchRequest = (state) => {
        patchIssue(id, {state})
            .then(() => setStatus(state))
            .catch((err) => setErrorMessage(err.message))
    }

    return (
        <li className="issue-list__element">
            <h2 className="issue-list__title">{title}</h2>

            <p>{description}</p>

            <select value={status} onChange={onChange}>
                {
                    statuses.map((name) => (
                        <option
                            key={`issue-${id}-option-${name}`}
                            value={name}
                        >
                            {name}
                        </option>
                    ))   
                }
            </select>
            {
                errorMessage && <span className="IssueList__errorMessage">{errorMessage}</span>
            }
        </li>
    )
}