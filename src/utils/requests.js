const BASE_URL = 'http://localhost:3001';

export const getIssues = () => fetch(`${BASE_URL}/issues`)
        .then((res) => res.json())

export const patchIssue = (id, body) => fetch(`${BASE_URL}/issues/${id}`, {
        method: 'PATCH',
        body: JSON.stringify(body),
        headers: {
                'Content-Type': 'application/json'
        },
})
.then((res) => res.json())
.then((data) => {
        if (data.data.errorMessage) throw new Error(data.data.errorMessage);
})