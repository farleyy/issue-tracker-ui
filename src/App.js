import React from 'react';
import './App.css';
import { IssueList } from './components';

function App() {
  return (
    <div className="Container">
      <header className="Header">
        <h1>Issue Tracker</h1>
      </header>

      <main>
        <IssueList/>
      </main>
    </div>
  );
}

export default App;
