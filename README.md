This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

**There are few thing that can be potentially improved/added:**  
 - eslint  
 - run tests and eslint on commit/push  
 - errors tracker (eg rollbar)  
 - sass or scss that allow for eg:  
   - store all possible colors in the one place  
   - faster development  
 - urls should be in separate file  
 - statuses of issues can be fetched from BE instead of hardcoded in the project  